﻿#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <cassert>
#include "matrix.h"

using namespace std;

rcMatrix::Matrix::Matrix(int Rows, int Columns)
{
	this->refCount = 1;
	this->rows = Rows;
	this->columns = Columns;
	this->size = Rows*Columns;
	this->cell = new double[this->size];

	//zerowanie tablicy
	for (int i = 0; i < this->size; i++) {
		this->cell[i] = 0;
	}
}

rcMatrix::Matrix::Matrix(string fileName){
	fstream file;
	file.open(fileName.c_str());
	this->refCount = 1;
	if (file.good()){
		cout << "Odczytywanie pliku" << endl;
		int x, y;
		double temp;
		if (file >> x >> y) {
			this->refCount = 1;
			this->size = x*y;
			this->rows = x;
			this->columns = y;
			this->cell = new double[this->size];
			for (y = 0; (y < this->size) && (file >> temp); y++) {
				this->cell[y] = temp;
			}
			if (y != this->size) {
				cout << "Blad odczytu\n";
				throw runtime_error("Blad odczytu");
			}
		}
		else {
			cout << "Blad odczytu\n";
			throw runtime_error("Blad odczytu");
		}
		file.close();
	}
	else {
		cout << "Nie udana proba otworzenia pliku\n";
		throw runtime_error("Nie mozna otworzyc pliku");
	}
}

rcMatrix::Matrix::Matrix() {
	this->refCount = 1;
	this->size = 0;
	this->rows = 0;
	this->columns = 0;
	this->cell = new double[this->size];
}
rcMatrix::Matrix::~Matrix() {
	delete[] this->cell;
}

rcMatrix::rcMatrix(int Rows, int Columns)
{
	matrix = new Matrix(Rows, Columns);
}
rcMatrix::~rcMatrix()
{
	cout <<matrix->refCount <<endl;
	if (--matrix->refCount == 0) {
		cout <<matrix->refCount<<" usuwam" <<endl;
		delete matrix;
	}
	cout <<"----------------------" <<endl;
}
rcMatrix::rcMatrix(string fileName)
{
	matrix = new Matrix(fileName);
}

rcMatrix::rcMatrix()
{
	matrix = new Matrix();
}

rcMatrix::rcMatrix(const rcMatrix &init)
{
	init.matrix->refCount++; //testiren dopieren
	matrix = init.matrix;
}
//funkcje

void rcMatrix::fill(int from, int to) {
	srand(time(NULL));
	if (to == 0) {
		to = from;
	}
	if (from > to) {
		int temp = from;
		from = to;
		to = temp;
	}
	to++;
	int size = abs(to - from);
	if (size == 0 && to == 0) {
		for (int i = 0; i < this->matrix->size; i++) {
			this->matrix->cell[i] = 0;
		}
	}
	else
	{
		double rndNum;
		for (int i = 0; i < this->matrix->size; i++) {
			rndNum = rand() % size + 0;
			this->matrix->cell[i] = rndNum + from;
		}
	}
}
int rcMatrix::cols() const{
	int cols = this->matrix->columns;
	return cols;
}
int rcMatrix::rows() const{
	int rows = this->matrix->rows;
	return rows;
}
void rcMatrix::dim() const{
	cout << "---dim---" << endl;
	cout << this->matrix->rows<<"x" << this->matrix->columns<< endl;
	cout << "---------" << endl;
	
}
double rcMatrix::read(int Rows, int Cols) const{
	int index = (this->matrix->columns)*Rows + Cols;
	return this->matrix->cell[index];
}
int rcMatrix::refCount() {
	return this->matrix->refCount;
}

//Operatory

ostream & operator<< (ostream &output, const rcMatrix data){

	int col_temp = data.matrix->columns-1;
	output << "---Matrix---" << endl;
	for (int i = 0, col_num = 0; i < data.matrix->size; i++,col_num++) {
		output << *(data.matrix->cell + i) << " ";
		if (col_num >= col_temp) {
			output << "\n";
			col_num = -1;
		}
	}
	//output << "------------" << endl;
	//output << "refCount: "<< data.matrix->refCount << endl;
	//output << "------------" << endl;
	return output;
}

//dodawanie
rcMatrix  operator + (const rcMatrix &matrix1, const rcMatrix &matrix2)
{
	if ((matrix1.matrix->rows == matrix2.matrix->rows) && (matrix1.matrix->columns == matrix2.matrix->columns)) {
		rcMatrix temp(matrix1.matrix->rows,matrix1.matrix->columns);
		for (int i = 0; i < matrix1.matrix->size; i++) {
			temp.matrix->cell[i] = matrix1.matrix->cell[i] + matrix2.matrix->cell[i];
		}
		return temp;
	}
	else {
		cout << "Wymiary macierzy się nie zgadzają\n";
		throw out_of_range("Niezgodne rozmiary macierzy");
		//return 0;
	}

}
//odejmowanie
rcMatrix  operator - (const rcMatrix &matrix1, const rcMatrix &matrix2)
{
	if ((matrix1.matrix->rows == matrix2.matrix->rows) && (matrix1.matrix->columns == matrix2.matrix->columns)) {
		rcMatrix temp(matrix1.matrix->rows, matrix1.matrix->columns);
		for (int i = 0; i < matrix1.matrix->size; i++) {
			temp.matrix->cell[i] = matrix1.matrix->cell[i] - matrix2.matrix->cell[i];
		}
		return temp;
	}
	else {
		cout << "Wymiary macierzy się nie zgadzają\n";
		throw out_of_range("Niezgodne rozmiary macierzy");
		//return 0;
	}

}
//porównywanie
bool operator == (const rcMatrix &matrix1, const rcMatrix &matrix2)
{
	if ((matrix1.matrix->rows == matrix2.matrix->rows) && (matrix1.matrix->columns == matrix2.matrix->columns)) {
		for (int i = 0; i < matrix1.matrix->size; i++) {
			if (matrix1.matrix->cell[i] != matrix2.matrix->cell[i]) {
				return false;
			}
		}
		return true;
	}
	else {
		return true;
	}
}

rcMatrix & rcMatrix::operator = (const rcMatrix &_matrix){
	if(--this->matrix->refCount == 0){
		delete this->matrix;
	}

	_matrix.matrix->refCount++;
	cout<< _matrix.matrix<<endl;
	this->matrix = _matrix.matrix;
	return *this;
}

rcMatrix & rcMatrix::operator += (const rcMatrix &_matrix){
	if (this->matrix->columns == _matrix.matrix->columns && this->matrix->rows == _matrix.matrix->rows)
	{
		Matrix *temp = new Matrix(this->matrix->rows, this->matrix->columns);
		for (int i = 0; i < _matrix.matrix->size; i++) {
			temp->cell[i] = this->matrix->cell[i] + _matrix.matrix->cell[i];
		}
		if (--this->matrix->refCount == 0) {
			delete this->matrix;
			cout << "'GC' Usuwanie matrixa\n";
		}
		this->matrix = temp;
	}
	else
		throw out_of_range("Niezgodne rozmiary macierzy");
	return *this;
}
rcMatrix & rcMatrix::operator -= (const rcMatrix &_matrix) {
	if (this->matrix->columns == _matrix.matrix->columns && this->matrix->rows == _matrix.matrix->rows)
	{
		Matrix *temp = new Matrix(this->matrix->rows, this->matrix->columns); //alokacja pamięci na nową macierz
		for (int i = 0; i < _matrix.matrix->size; i++) {
			temp->cell[i] = this->matrix->cell[i] - _matrix.matrix->cell[i];
		}
		if (--this->matrix->refCount == 0) {
			delete this->matrix;
			cout << "'GC' Usuwanie matrixa\n";
		}
		this->matrix = temp;
	}
	else
		throw out_of_range("Niezgodne rozmiary macierzy");
	return *this;
}
//mnożenie z przypisaniem
rcMatrix & rcMatrix::operator *= (const rcMatrix &_matrix) {
	if (this->matrix->columns == _matrix.matrix->rows)
	{
		Matrix *temp = new Matrix(this->matrix->rows, _matrix.matrix->columns); //alokacja pamięci na nową macierz
		for (int z = 0; z < this->matrix->rows; z++) {
			for (int x = 0; x < _matrix.matrix->columns; x++) {
				for (int y = 0; y < this->matrix->columns; y++) {
					//(*temp)(z, x) += ((*this)(z, y) * _matrix(y, x));
					(*temp).cell[temp->cellIndex(z, x)] += (this->matrix->cell[this->matrix->cellIndex(z, y)] * _matrix.matrix->cell[_matrix.matrix->cellIndex(y, x)]);
				}
			}
		}
		if (--this->matrix->refCount == 0) {
			delete this->matrix;
			cout << "'GC' Usuwanie matrixa\n";
		}
		this->matrix = temp;
		return *this;
	}
	else {
		throw out_of_range("Niezgodne rozmiary macierzy");
	}
	return *this;
}

//mnożenie
rcMatrix  operator * (const rcMatrix &_matrix1, const rcMatrix &_matrix2)
{
	_matrix1.dim();
	if (_matrix1.matrix->columns == _matrix2.matrix->rows)
	{
		rcMatrix temp(_matrix1.matrix->rows, _matrix2.matrix->columns);
		for (int z = 0; z < _matrix1.matrix->rows; z++) {
			for (int x = 0; x < _matrix2.matrix->columns; x++) {
				for (int y = 0; y < _matrix1.matrix->columns; y++) {
					temp.matrix->cell[temp.matrix->cellIndex(z,x)] += (_matrix1.matrix->cell[_matrix1.matrix->cellIndex(z,y)] * _matrix2.matrix->cell[_matrix2.matrix->cellIndex(y, x)]);
					//cout << "x: " << z << " y: " << x << endl;
				}
			}
		}
		return temp;
	}
	else {
		throw out_of_range("Niezgodne rozmiary macierzy");
	}
	//return 0;
}
//dodatkowo
int rcMatrix::Matrix::cellIndex(int Rows, int Cols) const {
	return this->columns*Rows + Cols;
}



double rcMatrix::index(int Rows, int Cols) const
{
	cout << "---------" << endl;
	cout << "Odczyt" << endl;
	cout << "---------" << endl;
	return this->matrix->cell[this->matrix->columns*Rows + Cols];
}

void rcMatrix::save(int Rows, int Cols, const double c)
{
	cout << "---------" << endl;
	cout << "Zapis" << endl;
	cout << "---------" << endl;
	//detach()
	if (this->matrix->refCount > 1) {
		//kopiowanie
		Matrix* temp = new Matrix(this->matrix->rows, this->matrix->columns);
		for (int i = 0; i < this->matrix->size; i++) {
			*(temp->cell + i) = *(this->matrix->cell + i);
		}
		this->matrix->refCount--;
		matrix = temp;

	}
	
	this->matrix->cell[this->matrix->columns*Rows + Cols] = c;
}
void copyMatrix(rcMatrix x, rcMatrix y) {

}


//CREF
rcMatrix::Cref rcMatrix::operator()(int Rows, int Cols)
{
	return Cref(*this, Rows, Cols);
}


rcMatrix::Cref::operator double() const
{
	return this->targetMat.index(Rows, Cols);
}

rcMatrix::Cref 	&rcMatrix::Cref::operator = (double number)
{
	this->targetMat.save(Rows, Cols, number);
	return *this;
}