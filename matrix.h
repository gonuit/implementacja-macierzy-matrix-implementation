﻿#ifndef MATRIX_H
#define MATRIX_H
#include<iostream>
#include<fstream>
#include<stdexcept>
#include<string.h>
using namespace std;


class rcMatrix
{

public:
	class Matrix;
	Matrix *matrix;
	class Cref; //CREF

	rcMatrix(string name);
	rcMatrix(int Rows, int Columns);
	rcMatrix();
	rcMatrix(const rcMatrix &matrix);
	~rcMatrix();

	void fill(int = 0, int = 0);
	int cols() const;
	int rows() const;
	void dim() const;


	double read(int n, int m) const; //odczyt
	friend ostream & operator<< (ostream &_output, const rcMatrix _data); //operator wyświetlania
	friend	rcMatrix  operator + (const rcMatrix &_matrix1, const rcMatrix &_matrix2); //operator dodawania
	friend	rcMatrix  operator - (const rcMatrix &_matrix1, const rcMatrix &_matrix2); //operator odejmowania
	friend	rcMatrix  operator * (const rcMatrix &_matrix1, const rcMatrix &_matrix2); //operator mnożenia
	bool friend operator == (const rcMatrix &_matrix1, const rcMatrix &_matrix2); //operator porównania
	rcMatrix & operator = (const rcMatrix &_matrix); //operator przypisania
	rcMatrix & operator += (const rcMatrix &_matrix); //operator dodawania z przypisaniem 
	rcMatrix & operator -= (const rcMatrix &_matrix); //operator odejmowania z przypisaniem
	rcMatrix & operator *= (const rcMatrix &_matrix); //operator mnożenia z przypisaniem;
	int refCount();

	double index(int Rows, int Cols) const; //dodatkowo
	void save(int Rows, int Cols, const double number);

	Cref operator()(int Rows, int Cols);
};

class rcMatrix::Matrix
{
public:

	double *cell;
	int refCount;
	int size;
	int rows;
	int columns;


	Matrix(string fileName); //odczyt z pliku
	Matrix(int Rows, int Columns);
	Matrix();
	~Matrix();
	int cellIndex(int Rows, int Cols) const; //dodatkowo
};

class rcMatrix::Cref
{
	friend class rcMatrix;
	int Rows;
	int Cols;
	rcMatrix &targetMat;
	Cref(rcMatrix &sourceMat, int srcRows, int srcCols): targetMat(sourceMat), Rows(srcRows), Cols(srcCols) {}
public:
	rcMatrix::Cref& operator = (double number);
	operator double() const;
};
#endif
