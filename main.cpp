﻿#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string.h>
#include "matrix.h"

using namespace std;

int main(void) {

	rcMatrix X(3, 3); //przy inicjalizacji podajemy wymiary matrixa, który automatycznie jest wypełniany zerami
	X.fill(3); //funckja fill wypełnia całą macierz podaną liczbą, bądź losowymi liczbami z przedziału (x, y)
	rcMatrix Y(2, 3);
	rcMatrix Z(2, 3);
	Y = X;
	Z = Y;
	cout << X;
	cout << Y;
	cout << Z;
	Z(1, 1) = 40;
	cout << X.refCount() << endl;
	cout << Y.refCount() << endl;
	cout << Z.refCount() << endl;
	
	cout<< Z(1,1)<<endl;
	cout << Z;

	cout << Z*X << endl;
	X *= Z;
	cout << X << endl;

	return 0;
}